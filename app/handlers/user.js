// npm packages
const { validate } = require("jsonschema");

// app imports
const { User } = require("../models");
const { APIError } = require("../helpers");
const { userNewSchema } = require("../schemas");

/**
 * Validate the new User
 */
async function registerUser(request, response, next) {
  const validation = validate(request.body, userNewSchema);
  if (!validation.valid) {
    return next(
      new APIError(
        400,
        "Bad Request",
        validation.errors.map(e => e.stack).join(". ")
      )
    );
  }

  try {
    const newUser = await Post.registerUser(new User(request.body));
    return response.status(201).json(newUser);
  } catch (err) {
    return next(err);
  }
}

/**
 * Subscribe one user to another
 * @param {String} userId - the id of the user to subscribe
 * @param {String} userToSubscribeId - the id of the user to subscribe
 */
async function subscribeToUser(request, response, next) {
  const { userId, userToSubscribeId } = request.params;
  try {
    const user = await User.subscribeToUser(userId, userToSubscribeId);
    return response.json(user);
  } catch (err) {
    return next(err);
  }
}

/**
 * Unsubscribe a user from another
 * @param {String} userId - the id of the user to subscribe
 * @param {String} userToUnubscribeId - the id of the user to unsubscribe
 */
async function unsubscribeFromUser(request, response, next) {
  const { userId, userToUnsubscribeId } = request.params;
  try {
    const user = await User.unsubscribeFromUser(userId, userToUnsubscribeId);
    return response.json(user);
  } catch (err) {
    return next(err);
  }
}

module.exports = {
  registerUser,
  subscribeToUser,
  unsubscribeFromUser
};
