// app imports
const { Post, User } = require("../models");
const { APIError, parseSkipLimit } = require("../helpers");

/**
 * List all the posts. Query params ?skip=0&limit=1000 by default
 */
async function getFeed(request, response, next) {
  let skip = parseSkipLimit(request.query.skip) || 0;
  let limit = parseSkipLimit(request.query.limit, 1000) || 1000;
  console.log("getting feed ", request.headers);
  try {
    const subscribedUsers = await User.getSubscribedUsers(request.headers.userid);
    const posts = await Post.getPosts({author: {"$in": subscribedUsers}}, {}, skip, limit);
    // posts = posts.sort((a, b) => b.date - a.date);
    return response.json(posts);
  } catch (err) {
    console.log("there was an error in getFeed: ", err);
    return next(err);
  }
}

module.exports = {
  getFeed
};
