// npm packages
const { validate } = require("jsonschema");

// app imports
const { Post } = require("../models");
const { APIError } = require("../helpers");
const { postNewSchema, postUpdateSchema } = require("../schemas");

/**
 * Validate the POST request body and create a new Post
 */
async function createPost(request, response, next) {
  const validation = validate(request.body, postNewSchema);
  if (!validation.valid) {
    return next(
      new APIError(
        400,
        "Bad Request",
        validation.errors.map(e => e.stack).join(". ")
      )
    );
  }

  try {
    const newPost = await Post.createPost(new Post(request.body));
    return response.status(201).json(newPost);
  } catch (err) {
    return next(err);
  }
}

/**
 * Get a single post
 * @param {String} id - the id of the post to retrieve
 */
async function getPost(request, response, next) {
  const { id } = request.params;
  try {
    const post = await Post.getPost(id);
    return response.json(post);
  } catch (err) {
    return next(err);
  }
}

/**
 * Update a single post
 * @param {String} id - the id of the Post to update
 */
async function updatePost(request, response, next) {
  const { id } = request.params;

  const validation = validate(request.body, postUpdateSchema);
  if (!validation.valid) {
    return next(
      new APIError(
        400,
        "Bad Request",
        validation.errors.map(e => e.stack).join(". ")
      )
    );
  }

  try {
    const post = await Post.updatePost(id, request.body);
    return response.json(post);
  } catch (err) {
    return next(err);
  }
}

/**
 * Remove a single post
 * @param {String} id - the id of the Post to remove
 */
async function deletePost(request, response, next) {
  const { id } = request.params;
  try {
    const deleteMsg = await Post.deletePost(id);
    return response.json(deleteMsg);
  } catch (err) {
    return next(err);
  }
}

module.exports = {
  createPost,
  getPost,
  updatePost,
  deletePost
};
