// npm packages
const mongoose = require("mongoose");

// const { v4: uuidv4 } = require('uuid');

// app imports
const { APIError } = require("../helpers");

// globals
const Schema = mongoose.Schema;

const subscriptionSchema = new Schema({
	userId: String,
	subscriptionTime: Date
});
const userSchema = new Schema({
  id: String,
  subscriptions: [subscriptionSchema],
  posts: [String]
});

userSchema.statics = {
  /**
   * Register a new user
   * @param {object} newUser - an instance of User
   * @returns {Promise<User, APIError>}
   */
  async registerUser(newUser) {
    const user = await newUser.save();
    return user.toObject();
  },
  /**
   * Register a user in another user's subscriptions
   * @param {String} subscriberId - user that subscribes
   * @param {String} userIdToSubscribe - the user to subscribe to
   * @returns {Promise<User, APIError>}
   */
  async subscribeToUser(subscriberId, userIdToSubscribe) {
    const user = await this.findOne({ id: subscriberId });

    if (!user) {
      throw new APIError(404, "User Not Found");
    }
    if (user.subscriptions.find((subscription) => subscription.userId == userIdToSubscribe)) {
      throw new APIError(400, "User already subscribed");
    }
    user.subscriptions.push({ userId: userToSubscribe._id, subscriptionTime: new Date() });
    await user.save();
    return user.toObject();
  },
  /**
   * Unregister a user from another user's subscriptions
   * @param {String} subscriberId - user that unsubscribes
   * @param {String} userIdToUnsubscribe - the user to unsubscribe from
   * @returns {Promise<User, APIError>}
   */
  async unsubscribeFromUser(subscriberId, userIdToUnsubscribe) {
    const user = await this.findOne({ id: subscriberId });

    if (!user) {
      throw new APIError(404, "User Not Found");
    }
    user.subscriptions = user.subscriptions.filter((subscription) => subscription.userId != userIdToUnsubscribe);
    await user.save();
    return user.toObject();
  },

  /**
   * Get list of subscriptions of a user
   * @param {String} userId - user to get subscriptions of
   * @returns {Promise<[Subscription], APIError>}
   */
  async getSubscribedUsers(userId) {
  	console.log("getSubscribedUsers " + userId);
    const user = await this.findOne({ id: userId });

    if (!user) {
      throw new APIError(404, "User Not Found");
    }
    return user.subscriptions.map(subscription => subscription.userId);
  }
};

/* Transform with .toObject to remove __v and _id from response */
if (!userSchema.options.toObject) userSchema.options.toObject = {};
userSchema.options.toObject.transform = (doc, ret) => {
  const transformed = ret;
  delete transformed.__v;
  return transformed;
};

/** Ensure MongoDB Indices **/
userSchema.index({ id: 1 }, { unique: true }); // example compound idx

module.exports = mongoose.model("User", userSchema);
