// npm packages
const mongoose = require("mongoose");

// const { v4: uuidv4 } = require('uuid');

// app imports
const { APIError } = require("../helpers");

// globals
const Schema = mongoose.Schema;

const postSchema = new Schema({
  content: String,
  author: String
});

postSchema.statics = {
  /**
   * Create a Single Post
   * @param {object} newPost - an instance of Post
   * @returns {Promise<Post, APIError>}
   */
  async createPost(newPost) {
    // newPost.id = uuidv4();
    const post = await newPost.save();
    return post.toObject();
  },
  /**
   * Delete a single Post
   * @param {String} id - the Post's id
   * @returns {Promise<Post, APIError>}
   */
  async deletePost(_id) {
    const deleted = await this.findOneAndRemove({ _id });
    if (!deleted) {
      throw new APIError(404, "Post Not Found");
    }
    return deleted.toObject();
  },
  /**
   * Get a single Post by id
   * @param {String} id - the Post's id
   * @returns {Promise<Post, APIError>}
   */
  async getPost(_id) {
    const post = await this.findOne({ _id });

    if (!post) {
      throw new APIError(404, "Post Not Found");
    }
    return post.toObject();
  },
  /**
   * Get a list of Posts
   * @param {Object} query - pre-formatted query to retrieve posts.
   * @param {Object} fields - a list of fields to select or not in object form
   * @param {String} skip - number of docs to skip (for pagination)
   * @param {String} limit - number of docs to limit by (for pagination)
   * @returns {Promise<Posts, APIError>}
   */
  async getPosts(query, fields, skip, limit) {
    const posts = await this.find(query, fields)
      .skip(skip)
      .limit(limit)
      .lean()
      // .sort({ name: 1 })
      .exec();
    if (!posts.length) {
      return [];
    }
    try{
      return posts.map((post) => {
        let { author, content } = post;
        return { author, content };
      });
    } catch (err) {
      console.log(err);
      return [];
    }
  },
  /**
   * Patch/Update a single Post
   * @param {String} id - the Post's id
   * @param {Object} postUpdate - the json containing the Post attributes
   * @returns {Promise<Post, APIError>}
   */
  async updatePost(_id, postUpdate) {
    const post = await this.findOneAndUpdate({ _id }, postUpdate, {
      new: true
    });
    if (!post) {
      throw new APIError(404, "Post Not Found");
    }
    return post.toObject();
  }
};

/* Transform with .toObject to remove __v and _id from response */
if (!postSchema.options.toObject) postSchema.options.toObject = {};
postSchema.options.toObject.transform = (doc, ret) => {
  const transformed = ret;
  delete transformed.__v;
  return transformed;
};

/** Ensure MongoDB Indices **/
postSchema.index({ id: 1 }, { unique: true }); // example compound idx

module.exports = mongoose.model("Post", postSchema);
