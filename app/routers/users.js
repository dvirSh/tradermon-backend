// npm packages
const express = require("express");

// app imports
const { userHandler } = require("../handlers");

// globals
const router = new express.Router();
const { registerUser, subscribeToUser, unsubscribeFromUser } = userHandler;

/* User page */
// router
//   .route("/:id")
//   .get(getUser)
//   .post(registerUser);

/* User subscription */
router
  .route("/:id/subscribe")
  .post(subscribeToUser);

/* User subscription */
router
  .route("/:id/unsubscribe")
  .post(unsubscribeFromUser);

module.exports = router;
