// npm packages
const express = require("express");

// app imports
const { postHandler, postsHandler } = require("../handlers");

// globals
const router = new express.Router();
const { getPosts } = postsHandler;
const { createPost, getPost, updatePost, deletePost } = postHandler;

/* All the Posts Route */
router
  .route("")
  .get(getPosts)
  .post(createPost);

/* Single Post by Id Route */
router
  .route("/:id")
  .get(getPost)
  .patch(updatePost)
  .delete(deletePost);

module.exports = router;
