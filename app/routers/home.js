// npm packages
const express = require("express");

// app imports
const { feedHandler } = require("../handlers");

// globals
const router = new express.Router();
const { getFeed } = feedHandler;

/* Home page - feed */
router
  .route("")
  .get(getFeed);

module.exports = router;
