/**
 * These tests currently only work if you have a local MongoDB database
 */
const app = require("../app/app");
const request = require("supertest");
const mongoose = require("mongoose");

let { Post, User } = require("../app/models");
let examplePost = {
  content: "Sweet sweat",
  author: "Meziadayda"
};
let id = null;
let user1 = {
  id: "Meziadayda"
};

beforeEach(async () => {
  const testPost = new Post(examplePost);
  id = testPost._id;
  console.log("saving post: " + id)
  await testPost.save();
  const testUser1 = new User(user1);
  await testUser1.save();
});

afterEach(async () => {
  await mongoose.connection.dropCollection("posts");
  await mongoose.connection.dropCollection("users");
});

afterAll(async () => {
  // CLEAN UP
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("GET /posts", () => {
  test("Get a list of posts", async () => {
    let response = await request(app).get("/posts");
    expect(response.body.length).toEqual(1);
    expect(response.body[0].content).toEqual(examplePost.content);
  });

  test("Get a list of posts by a specific user", async () => {
    let response = await request(app).get("/posts?author=Dardasaba");
    expect(response.body.length).toEqual(0);
    let response2 = await request(app).get("/posts?author=Meziadayda");
    expect(response2.body.length).toEqual(1);
    expect(response2.body[0].author).toEqual(examplePost.author);
  });
});

describe("POST /posts", () => {
  test("Create a new Post", async () => {
    let response = await request(app)
      .post("/posts")
      .send({ content: "A Post", author: "Sumo" });
    expect(response.body).toHaveProperty("_id");
    expect(response.body.content).toEqual("A Post");
    expect(response.body.author).toEqual("Sumo");
  });
});

describe("PATCH /posts/:_id", () => {
  test("Update a post's content", async () => {
    let response = await request(app)
      .patch("/posts/" + id)
      .send({ content: "new content" });
    expect(response.body.content).toEqual("new content");
  });
});

describe("DELETE /posts/:_id", () => {
  test("Delete a post by _id", async () => {
    console.log("delete "+ id)
    let response = await request(app).delete("/posts/" + id);
    // console.log(response)
    expect(response.body.content).toEqual(examplePost.content);
  });
});

describe("GET /", () => {
  beforeEach(async () => {
    const testUser = new User({ id: "NewUser", subscriptions: [
      { userId: "Meziadayda", subscriptionTime: new Date() }
    ] });
    await testUser.save();
  });

  test("Get all posts from all subscribed users", async () => {
    let response = await request(app).get("/").set("userId", "NewUser");
    expect(response.statusCode).toEqual(200);
    expect(response.body.length).toEqual(1);
    expect(response.body[0].content).toEqual(examplePost.content);
  });
});
